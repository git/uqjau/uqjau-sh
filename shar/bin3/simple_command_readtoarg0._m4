#!/usr/bin/env perl
# --------------------------------------------------------------------
# Synopsis: read simple command, identifying env assignments
# and command basename. (see 'man bash' for definition of
# "simple command") Invoke w/${0} == simple_command_basename or
# simple_command_env_assignments to get behavior wanted.
# --------------------------------------------------------------------
# Other Names ( symbolic links in commands dir ) : 
#   simple_command_basename
#   simple_command_env_assignments
# --------------------------------------------------------------------
# Usage: 
#   case 1:
#     simple_command_basename SIMPLE_SHELL_COMMAND_HERE
#       # outputs basename of simple command
#   case 2:
#     simple_command_env_assignments SIMPLE_SHELL_COMMAND_HERE
#       # lists each env assignment, one per line
#
# --------------------------------------------------------------------
# Rating: tone: script-support tool used: yes stable: y TBDs: y interesting: y ontology/tags: shell, jobs noteworthy: y
# --------------------------------------------------------------------
# Where Used: jobmon, _dirdefs.static
# --------------------------------------------------------------------

# FYI:
  # File: bash.info,  Node: Simple Commands,  Next: Pipelines,  Up: Shell Commands
  # 
  # 3.2.1 Simple Commands
  # ---------------------
  # 
  # A simple command is the kind of command encountered most often.  It's
  # just a sequence of words separated by `blank's, terminated by one of
  # the shell's control operators (*note Definitions::).  The first word
  # generally specifies a command to be executed, with the rest of the
  # words being that command's arguments.

# ====================================================================
# Copyright (c) Jun 2009 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2009/10/21 13:28:54 $   (GMT)
# $Revision: 1.5 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/sh/shar/bin/RCS/simple_command_readtoarg0,v $
#      $Log: simple_command_readtoarg0,v $
#      Revision 1.5  2009/10/21 13:28:54  rodmant
#      *** empty log message ***
#
#      Revision 1.4  2009/05/27 13:47:49  rodmant
#      *** empty log message ***
#
#      Revision 1.3  2009/05/27 13:35:12  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

sub read_simple_command_for_env_assmnts_and_command {

  # -------------------------------------------------------------------- 
  # Synopsis: Read command line, with possible leading env var
  # assignments, to identify command to be run.  Arguments assumed
  # to be a "simple command" as defined in 'man bash'
  # -------------------------------------------------------------------- 

  # assumption: no argument includes a "\n"
  # Ex this legal command is not allowed:
  #   echo "hi
  #   there"
  # low priority TBD: think about how to remove above limitation

  my $command; 
  my $CL_arg;

  foreach (@_) 
    {
      $CL_arg=$_;

      if 
      ($CL_arg =~
        m{
             ^                 # start of arg
             [a-zA-Z_]         # bash var name cannot start w/a digit
             \w*               # \w == wordchar == [0-9a-z_A-Z]
             =
                               # chars after = are don't care (we're
                               # not checking shell syntax, that's not our job )
        }x 
      )
        { 
          push (@env_assnmnts,$CL_arg); 
          next; 
        }

     # env assignments if any are DONE, so *this* arg is the
     # command itself

     $command = "$CL_arg"; 

     last;
       # we do not care about any arguments after the command

    }

  # --
  # TBD/fix?: our behavior controlled by a global env var "$only_show_env_assnmnts"

  if (! $only_show_env_assnmnts) 
    { 
      return $command;
    }
  else
    { 
      if (! @env_assnmnts) 
        { 
          @env_assnmnts = (); 
        }

      return @env_assnmnts;
    }

}

# ********************************************************************
# main 
# ********************************************************************
our $ourname;
($ourname  = $0) =~ s:.*/::g;
  # our basename

# change our behavior depending on name we are invoked by
for ($ourname)
  {
    if (/^simple_command_env_assignments$/)
      {
        $only_show_env_assnmnts = 1;
        if (@out_list = read_simple_command_for_env_assmnts_and_command @ARGV )
          { 
            print join("\n",@out_list) . "\n";
            exit 0;
          }
        else
          { 
            exit 1;
          }
      }
    elsif (/^simple_command_basename$/)
      {
        (
          $command_basename = 
          read_simple_command_for_env_assmnts_and_command @ARGV 
        ) =~ s:^.*/::;

        if ($command_basename) 
          {
            print "$command_basename\n";
            exit 0;
          }
        else 
          {
            exit 2;
          }

      }
  }
