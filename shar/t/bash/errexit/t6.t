#!/bin/bash 

is()
{
  if [[ ! $# == 3 ]];then
    echo $FUNCNAME:ERROR: exactly 3 args required > &2
    return 1
  fi

  local expected=$1 computed=$2 testName=$3
  local status
  
  # globals: isTestCount isPassCount
  isTestCount=${isTestCount:-0}
  isPassCount=${isPassCount:-0}

  let isTestCount++ || :

  if [[ $expected == $computed ]];then
    let isPassCount++ || :
    status=ok
    printf "%s  %d - %s\n" "$status" "$isTestCount" "$testName"
  else
    status="not ok"
    printf "%s  %d - %s\n" "$status" "$isTestCount" "$testName"
    printf "  expected: [$expected]"
    printf "  computed: [$computed]"
  fi

}

: Under 'set -e' bash should exit after a "simple command" fails /eg the 'false'
: command should trip "errexit"/. Function "foo" has 'set -e', here is "good/expected" behavior:
bash -c '(set -e; foo(){ echo hi;false;echo $FUNCNAME: after false; };foo; echo bye)'
:
: Any test case below showing "foo: after false" I consider "broken".
: not a bug /these are not "simple commands"/, just not what you want/expect:
:
: "! foo" broken when function "foo" contains 'set -e':
bash -c '(set -e; foo(){ echo hi;false;echo $FUNCNAME: after false; }; ! foo ; echo bye)'
:
: "if foo" broken when function "foo" contains 'set -e':
bash -c '(set -e; foo(){ echo hi;false;echo $FUNCNAME: after false; }; if foo;then :;fi;echo bye)'
:
: "foo || true" broken when function "foo" contains 'set -e':
bash -c '(set -e; foo(){ echo hi;false;echo $FUNCNAME: after false; }; foo || true ; echo bye)'
:
: "foo && false" broken when function "foo" contains 'set -e':
bash -c '(set -e; foo(){ echo hi;false;echo $FUNCNAME: after false; }; foo && true ; echo bye)'
:
: It would be nice if you could run 'foo', so that 'set -e' results in aborting 'foo'
: right after the 'false' command, but *not* aborting the shell that defines 'foo'.
: The below workaround is a less then ideal, but best that I can find:
bash -c 'set -e;foo(){ echo hi;false;echo $FUNCNAME: after false;}; foo|cat; echo bye ${PIPESTATUS[0]}'



